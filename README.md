# Timetable Parser For SDM

This project is a browser automation script to scrape timetable entries from Swansea University College of Science's
timetable.

## Installation

The pre-requisites are:

- OpenJDK 11.0.10+ or equivalent

Clone or download the source code into a new directory such as `/home/<user>/sdm-scraper/`.

## Running The Script

Open `src/main/resources/config.properties` and configure `startUrl` and `weeksToScrape`.

**Note:** The `startUrl` is the URL of the timetable page that shows all timetable entries for the
department. `weeksToScrape` is the number of weeks including the first one to be scraped.

![](docs/images/start_url.png)

Open `src/main/resources/credentials.properties` and configure your credentials to access the College of Science
timetable.

**Note:** Remember to remove these when you are finished scraping.

Run the script:

```bash
./gradlew run
```

**Note:** It will open a browser window and navigate through the selected number of weeks automatically. Do not
interfere with that browser window.

When the script finishes, a new file will be generated in the `export` directory with a name containing the date it was
scraped. This file is ready to be imported in
the [Student Demonstrator Manager](https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/web-system) web system.

## Miscellaneous

The export format is described in
the [SDM documentation](https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/web-system/-/blob/master/docs/DOCUMENTATION.md)
in detail, but may look something like this:

```csv
"DURATION","ROOM"                         ,"START"           ,"TITLE"               ,"TYPE"   ,"UNIID"
"PT1H"    ,"Zoom ID: 4177612215"          ,"2020-09-28T09:00","CSC318 Lab Selection","LECTURE","40121"
"PT1H"    ,"Zoom ID: 4177612215"          ,"2020-09-28T09:00","CSCM18 Lab Selection","LECTURE","40122"
"PT1H"    ,"Online Class"                 ,"2020-09-28T10:00","CS-110"              ,"LECTURE","39829"
"PT2H"    ,"Online Class"                 ,"2020-09-28T10:00","CS-250"              ,"LECTURE","39857"
"PT2H"    ,"School of Management 128 (PC)","2020-09-28T10:00","CSC345 Group A"      ,"LAB"    ,"39468"
"PT2H"    ,"School of Management 128 (PC)","2020-09-28T10:00","CSCM45 Group A"      ,"LAB"    ,"39469"
"PT2H"    ,"Grt Hall 037"                 ,"2020-09-28T10:00","CSCM69"              ,"LECTURE","39444"
"PT1H"    ,"Online Class"                 ,"2020-09-28T11:00","CS-170"              ,"LECTURE","39879"
"PT2H"    ,"School of Management 128 (PC)","2020-09-28T12:00","CSC345 Group B"      ,"LAB"    ,"39466"
"PT2H"    ,"School of Management 128 (PC)","2020-09-28T12:00","CSCM45 Group B"      ,"LAB"    ,"39467"
```

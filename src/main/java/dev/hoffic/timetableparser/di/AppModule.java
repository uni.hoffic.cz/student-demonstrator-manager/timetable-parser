package dev.hoffic.timetableparser.di;

import com.google.inject.AbstractModule;
import dev.hoffic.timetableparser.App;
import dev.hoffic.timetableparser.io.Exporter;
import dev.hoffic.timetableparser.processing.MultiHourProcessor;
import dev.hoffic.timetableparser.recipe.AuthenticateRecipe;
import dev.hoffic.timetableparser.recipe.ScrapeTimetableEntriesRecipe;
import dev.hoffic.timetableparser.util.DriverProvider;

public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(App.class);
        bind(AuthenticateRecipe.class);
        bind(DriverProvider.class);
        bind(ScrapeTimetableEntriesRecipe.class);
        bind(MultiHourProcessor.class);
        bind(Exporter.class);
    }
}

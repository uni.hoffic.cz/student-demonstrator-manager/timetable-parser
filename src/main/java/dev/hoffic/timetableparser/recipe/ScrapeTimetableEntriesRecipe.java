package dev.hoffic.timetableparser.recipe;

import com.google.inject.Inject;
import dev.hoffic.timetableparser.entity.Timetable;
import dev.hoffic.timetableparser.io.AppConfig;
import dev.hoffic.timetableparser.processing.TimetableEntryParser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScrapeTimetableEntriesRecipe {

    @Inject
    private TimetableEntryParser timetableEntryParser;
    @Inject
    private AppConfig appConfig;

    public void play(ChromeDriver driver, Timetable timetable) {

        for (int i = 0; i < appConfig.getWeeksToScrape(); i++) {
            var timetableSlots = driver.findElementById("timetable").findElements(By.className("slot"));

            for (WebElement webElement : timetableSlots) {
                timetableEntryParser.parseTimetableEntries(webElement).forEach(timetable::add);
            }

            driver.findElementById("rightlink").click();
        }
    }
}

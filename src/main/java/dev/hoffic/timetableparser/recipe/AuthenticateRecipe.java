package dev.hoffic.timetableparser.recipe;

import com.google.inject.Inject;
import dev.hoffic.timetableparser.io.AppConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AuthenticateRecipe {

    @Inject
    AppConfig appConfig;

    public void play(WebDriver driver) {
        driver.get(appConfig.getStarUrl());
        driver.findElement(By.id("id_username")).sendKeys(appConfig.getUsername());
        driver.findElement(By.id("id_password")).sendKeys(appConfig.getPassword());
        driver.findElement(By.id("loginbutton")).click();
    }
}

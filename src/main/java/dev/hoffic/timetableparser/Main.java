package dev.hoffic.timetableparser;

import com.google.inject.Guice;
import dev.hoffic.timetableparser.di.AppModule;

public class Main {

    public static void main(String[] args) {
        var injector = Guice.createInjector(new AppModule());
        Runnable app = injector.getInstance(App.class);
        app.run();
    }
}

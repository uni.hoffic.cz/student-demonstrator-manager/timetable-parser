package dev.hoffic.timetableparser.e9n;

public enum TimetableEntryTypeEnum {
    LAB,
    LECTURE,
    OTHER
}

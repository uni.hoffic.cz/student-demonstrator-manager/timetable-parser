package dev.hoffic.timetableparser.util;

import com.google.inject.Provider;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverProvider implements Provider<ChromeDriver> {

    @Override
    public ChromeDriver get() {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }
}

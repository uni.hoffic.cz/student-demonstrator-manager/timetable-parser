package dev.hoffic.timetableparser;

import com.google.inject.Inject;
import dev.hoffic.timetableparser.entity.Timetable;
import dev.hoffic.timetableparser.io.Exporter;
import dev.hoffic.timetableparser.processing.MultiHourProcessor;
import dev.hoffic.timetableparser.recipe.AuthenticateRecipe;
import dev.hoffic.timetableparser.recipe.ScrapeTimetableEntriesRecipe;
import dev.hoffic.timetableparser.util.DriverProvider;
import org.openqa.selenium.chrome.ChromeDriver;

public class App implements Runnable {

    @Inject
    private DriverProvider driverProvider;
    @Inject
    private AuthenticateRecipe authenticateRecipe;
    @Inject
    private ScrapeTimetableEntriesRecipe scrapeTimetableEntriesRecipe;
    @Inject
    private MultiHourProcessor multiHourProcessor;
    @Inject
    private Exporter exporter;

    @Override
    public void run() {
        ChromeDriver driver = null;

        try {
            // Setup
            driver = driverProvider.get();
            var timetable = new Timetable();

            // Scraping
            authenticateRecipe.play(driver);
            scrapeTimetableEntriesRecipe.play(driver, timetable);

            // Processing
            timetable = multiHourProcessor.removeRepeatingEntries(timetable);

            // Exporting
            exporter.exportCsv(timetable);

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            System.exit(1);

        } finally {
            if (driver != null) {
                driver.quit();
            }
        }
    }
}

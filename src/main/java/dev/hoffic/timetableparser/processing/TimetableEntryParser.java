package dev.hoffic.timetableparser.processing;

import dev.hoffic.timetableparser.e9n.TimetableEntryTypeEnum;
import dev.hoffic.timetableparser.entity.TimetableEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class TimetableEntryParser {
    public List<TimetableEntry> parseTimetableEntries(WebElement slotWebElement) {
        var start = LocalDateTime.of(
                Integer.parseInt(slotWebElement.getAttribute("data-year")),
                Integer.parseInt(slotWebElement.getAttribute("data-month")),
                Integer.parseInt(slotWebElement.getAttribute("data-day-of-month")),
                Integer.parseInt(slotWebElement.getAttribute("data-hour")),
                0
        );

        var entryWebElements = slotWebElement.findElements(By.className("lecture"));

        var entries = new LinkedList<TimetableEntry>();

        for (var entryWebElement : entryWebElements) {
            entries.add(
                    parseTimetableEntry(
                            entryWebElement,
                            start));
        }

        return entries;
    }

    private TimetableEntry parseTimetableEntry(WebElement webElement, LocalDateTime start) {
        return new TimetableEntry(
                Integer.parseInt(webElement.getAttribute("data-lecture-pk")),
                webElement.findElement(By.tagName("strong")).getText(),
                parseType(webElement),
                start,
                parseDuration(webElement),
                webElement.findElement(By.className("room")).getText()
        );
    }

    private Duration parseDuration(WebElement webElement) {
        try {
            var duration = webElement.findElement(By.className("duration")).getText();

            var pattern = Pattern.compile("(\\d{1,2}):\\d{2} hours");
            var matcher = pattern.matcher(duration);

            if (matcher.matches()) {
                return Duration.ofHours(Integer.parseInt(matcher.group(1)));
            } else {
                return Duration.ofHours(1);
            }
        } catch (NoSuchElementException e) {
            return Duration.ofHours(1);
        }
    }

    private String parseType(WebElement webElement) {
        var title = webElement.getAttribute("title");

        if (title.contains("Practical")) {
            return TimetableEntryTypeEnum.LAB.name();
        } else if (title.contains("Lecture")) {
            return TimetableEntryTypeEnum.LECTURE.name();
        } else {
            return TimetableEntryTypeEnum.OTHER.name();
        }
    }
}

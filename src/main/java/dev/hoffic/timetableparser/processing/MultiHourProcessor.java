package dev.hoffic.timetableparser.processing;

import com.google.inject.Inject;
import dev.hoffic.timetableparser.entity.Timetable;
import dev.hoffic.timetableparser.entity.TimetableEntry;
import dev.hoffic.timetableparser.io.AppConfig;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;

public class MultiHourProcessor {

    @Inject
    private AppConfig appConfig;

    public Timetable removeRepeatingEntries(Timetable rawTimetable) {
        var processedTimetable = new Timetable();
        var timetableEntryBans = new HashMap<String, LocalDateTime>();

        var sortedEntries = rawTimetable.getAll();
        sortedEntries.sort(Comparator.comparing(TimetableEntry::getStart));

        for (var entry : sortedEntries) {
            var key = entry.getTitle() + "@" + (appConfig.isDistinguishRoom() ? entry.getRoom() : "");
            timetableEntryBans.putIfAbsent(key, LocalDateTime.MIN);

            var bannedUntil = timetableEntryBans.get(key);
            if (!entry.getStart().isBefore(bannedUntil)) {
                processedTimetable.add(entry);
                bannedUntil = entry.getStart().plus(entry.getDuration());
                timetableEntryBans.replace(key, bannedUntil);
            }
        }

        return processedTimetable;
    }
}

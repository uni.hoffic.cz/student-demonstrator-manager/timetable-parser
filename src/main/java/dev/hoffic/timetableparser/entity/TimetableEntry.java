package dev.hoffic.timetableparser.entity;

import com.google.common.base.MoreObjects;
import com.opencsv.bean.CsvBindByName;

import java.time.Duration;
import java.time.LocalDateTime;

public class TimetableEntry {

    @CsvBindByName
    private final int uniId;

    @CsvBindByName
    private final String title;

    @CsvBindByName
    private final String type;

    @CsvBindByName
    private final LocalDateTime start;

    @CsvBindByName
    private final Duration duration;

    @CsvBindByName
    private final String room;

    public TimetableEntry(
            int uniId,
            String title,
            String type,
            LocalDateTime start,
            Duration duration,
            String room
    ) {
        this.uniId = uniId;
        this.title = title;
        this.type = type;
        this.start = start;
        this.duration = duration;
        this.room = room;
    }

    public int getUniId() {
        return uniId;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public Duration getDuration() {
        return duration;
    }

    public String getRoom() {
        return room;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uniId", uniId)
                .add("title", title)
                .add("type", type)
                .add("start", start)
                .add("duration", duration)
                .addValue("\n")
                .toString();
    }
}

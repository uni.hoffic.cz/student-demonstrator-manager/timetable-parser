package dev.hoffic.timetableparser.entity;

import java.util.LinkedList;
import java.util.List;

public class Timetable {
    LinkedList<TimetableEntry> timetableEntries;

    public Timetable() {
        this.timetableEntries = new LinkedList<>();
    }

    public void add(TimetableEntry timetableEntry) {
        timetableEntries.add(timetableEntry);
    }

    public List<TimetableEntry> getAll() {
        return new LinkedList<>(timetableEntries);
    }

    @Override
    public String toString() {
        var stringBuilder = new StringBuilder();

        timetableEntries.forEach(stringBuilder::append);

        return stringBuilder.toString();
    }
}

package dev.hoffic.timetableparser.io;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import dev.hoffic.timetableparser.entity.Timetable;
import dev.hoffic.timetableparser.entity.TimetableEntry;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Exporter {

    public void exportCsv(Timetable timetable) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        var writer = new FileWriter(generateFilename());
        StatefulBeanToCsv<TimetableEntry> sbc = new StatefulBeanToCsvBuilder<TimetableEntry>(writer)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();

        for (TimetableEntry timetableEntry : timetable.getAll()) {
            sbc.write(timetableEntry);
        }

        writer.close();
    }

    private String generateFilename() {
        return "export/export_"
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm"))
                + ".csv";
    }
}

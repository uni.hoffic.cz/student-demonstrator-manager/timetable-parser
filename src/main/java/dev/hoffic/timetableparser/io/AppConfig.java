package dev.hoffic.timetableparser.io;

import java.io.IOException;
import java.util.Properties;

public class AppConfig {

    Properties credentials;
    Properties config;

    public AppConfig() {
        credentials = new Properties();
        config = new Properties();

        try {
            credentials.load(ClassLoader.getSystemClassLoader().getResourceAsStream("credentials.properties"));
            config.load(ClassLoader.getSystemClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            System.err.println("Unable to read properties file.");
            System.exit(1);
        }
    }

    public String getUsername() {
        return credentials.getProperty("username");
    }

    public String getPassword() {
        return credentials.getProperty("password");
    }

    public String getStarUrl() {
        return config.getProperty("startUrl");
    }

    public int getWeeksToScrape() {
        return Integer.parseInt(config.getProperty("weeksToScrape"));
    }

    public boolean isDistinguishRoom() {
        return "true".equals(credentials.getProperty("distinguishRoom"));
    }
}
